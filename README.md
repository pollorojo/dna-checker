# DNA mutations

## Getting started
*Install dependencies*
```bash
npm install
```

## Test
*Execute testing*
```bash
npm run test
```

## Run project
To run project execute in terminal:
```bash
npm run start
```
Then you can use any Http Client like **Postman** to make a POST request to http://127.0.0.1:3000/mutation or http://localhost:3000/mutation

The body of request should has similar schema:
```json
{
    data: ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}
```

Also you can run de scripts to see an example of endpoint implementation:

**Test DNA with mutation, run:**
```bash
./test-1.sh
```

**Test DNA without mutation, run:**
```bash
./test-2.sh
```

## Request Examples
---

## **DNA with mutation**

```bash
curl -i --header "Content-Type: application/json" \
    --request POST \
    --data '@test-1.json' \
    https://wodwrg4hj0.execute-api.us-east-2.amazonaws.com/mutation
```

**Response:**

>>>
HTTP/2 200 

**date:** Mon, 08 Feb 2021 03:43:10 GMT

**content-length:** 0

**x-powered-by:** Express

**access-control-allow-origin:** *

**access-control-allow-headers:** Origin, X-Requested-With, Content-Type, Accept
>>>


## **DNA without mutation**

```bash
curl -i --header "Content-Type: application/json" \
    --request POST \
    --data '@test-2.json' \
    https://wodwrg4hj0.execute-api.us-east-2.amazonaws.com/mutation
```

>>>
HTTP/2 403 

**date:** Mon, 08 Feb 2021 03:48:18 GMT

**content-length:** 0

**x-powered-by:** Express

**access-control-allow-origin:** *

**access-control-allow-headers:** Origin, X-Requested-With, Content-Type, Accept
>>>