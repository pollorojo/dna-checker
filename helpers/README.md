# Helpers

## Arr Module
*Provide methods for array handle and inspection.*

To use this helper, first import it.
```javascript
const arr = require('../helpers/arr');
```

### **arrayDictionaryMapper(data: String[])**
*Create dictionary structure to index the repetitions for each char from array string.*
```javascript
var data = ['a','a','b','c','c','c'];
var result = arr.arrayDictionaryMapper(data);
console.log(result); // print {a: 2, b: 1, c: 3}
```

### **stringDictionaryMapper(sequence: String)**
*Create dictionary structure to index the repetitions for each char from a string.*
```javascript
var data = 'aabccc';
var result = arr.stringDictionaryMapper(data);
console.log(result); // print {a: 2, b: 1, c: 3}
```

### **hasGteValue(obj: Object, max: integer)**
*Check if has any greater or equal integer value to max.*
```javascript
var data = {'a': 4, 'b': 1, 'c': 3};
var result = arr.hasGteValue(data, 4);
console.log(result); // print true
```

### **countCharRepetition(arr: Object[], max: integer)**
*Count items that contain greater or equal of max value.*
```javascript
var data = [
    {'a':3,'b':4,'c':1},
    {'a':4,'b':2,'c':1},
    {'a':3,'b':2,'c':1}
];
var result = arr.countCharRepetition(data, 4);
console.log(result); // print 2
```

### **invertMatrix(values: Array[])**
*Re-arrange matrix to alternate vertical values to horizontal values..*
```javascript
var data = [
    [1,3,5,7],
    [2,4,6,8],
    ['a','c','e','g'],
    ['b','d','f','h']
];
var result = arr.invertMatrix(data);
console.log(result); 
/* 
print: [
    [1,2,'a','b'],
    [3,4,'c','d'],
    [5,6,'e','f'],
    [7,8,'g','h']
]
*/
```

----