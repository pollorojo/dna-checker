/**
 * Create dictionary structure to index the repetitions for each char.
 * 
 * @param Array arr 
 */
module.exports.arrayDictionaryMapper = (arr) => {
    let charMap = {};

    for (let item of arr) {
        if (charMap.hasOwnProperty(item)) {
            charMap[item]++
        } else {
            charMap[item] = 1
        }
    }

    return charMap;
}

/**
 * Map data to dictionary data structure.
 * 
 * @param String sequence 
 */
module.exports.stringDictionaryMapper = (sequence) => {
    let chars = (new String(sequence)).split('');
    return this.arrayDictionaryMapper(chars);
}

/**
 * Check if has any greater or equal integer value to max.
 * 
 * @param Object obj 
 * @param integer max 
 */
module.exports.hasGteValue = (obj, max) => {
    const values = Object.values(obj).sort((a, b) => b - a);
    return values[0] >= max;
}

/**
 * Check if exists any integer value greater or equal, from the parameter received, in array.
 * 
 * @param Array data
 * @param integer max 
 * @return boolean 
 */
module.exports.countCharRepetition = (data, max) => {
    let count = 0;

    for (let index = 0; index < data.length; index++) {
        if (this.hasGteValue(data[index], max)) {
            count++;
        }
    }

    return count;
}

/**
 * Re-arrange matrix to alternate vertical values to horizontal values.
 * 
 * @param Array values 
 */
module.exports.invertMatrix = (values) => {
    let arr = new Array(values.length);

    for (let index = 0; index < values.length; index++) {     
        
        for (let i = 0; i < values[index].length; i++) {
            if (arr[i] == undefined) {
                arr[i] = new Array(values.length);
            }

            arr[i][index] = values[index][i];
        }
    }

    return arr;
}