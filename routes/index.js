var express = require('express');
var router = express.Router();

const controller = require('../controllers/dna.controller');

/* GET home page. */
router.post('/mutation', controller.inspect_dna);

module.exports = router;