const DnaService = require('../services/dna.service');

module.exports.inspect_dna = function(req, res) {
    const data = req.body;

    if (data.dna != undefined) {
        const service = new DnaService();

        return service.hasMutation(data.dna)
        .then(function (result) {
            var responseStatus = (result) ? 200 : 403;
            return res.status(responseStatus).send();
        }, (errorMsg) => {
            return res.status(400).json({
                error: 'Bad Request',
                message: errorMsg
            });
        }).catch(function (err) {
            // never goes here
            console.log(err);
            return res.status(500).json({
                error: 'Server Error'
            });
        });
    } else {
        res.status(400).json({
            error: 'Bad Request',
            message: 'Key "dna" not found in body request.'
        });
    }
};