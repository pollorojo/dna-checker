const serverless = require('serverless-http');
var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRoute = require('./routes/index');

var app = express();

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');

    // authorized headers for preflight requests
    // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRoute);

app.options('*', (req, res) => {
    // allowed XHR methods  
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    res.send();
});

module.exports = app;
module.exports.handler = serverless(app);