const assert = require( "chai" ).assert;
const mocha = require( "mocha" );
const DnaService = require( "../../../services/dna.service" ); // Import the service we want to test

mocha.describe( "DNA Service", () => {
    const DnaServiceInstance = new DnaService();

    async function assertThrowsAsync(fn, regExp) {
        let f = () => {};
        try {
            await fn();
        } catch(e) {
            f = () => {throw e};
        } finally {
            assert.throws(f, regExp);
        }
    }

    mocha.describe( "Check mutations in DNA", () => {
        it( "Validate correct sequence", async () => {
            let result = DnaServiceInstance.hasValidNitrogenousBases('TCACTG');
            
            assert.isTrue( result );
        });

        it( "Validate incorrect sequence", async () => {
            let result = DnaServiceInstance.hasValidNitrogenousBases('TCADTG');
            
            assert.isFalse( result );
        });

        it( "Validate corrent matrix dim", async () => {
            let result = DnaServiceInstance.hasValidMatrixDim([[1,2,3],[1,2,3],[1,2,3]]);
            
            assert.isTrue( result );
        });

        it( "Validate incorrent matrix dim", async () => {
            let result = DnaServiceInstance.hasValidMatrixDim([[1,2,3],[1,2],[1,2,3]]);
            
            assert.isFalse( result );
        });

        it( "Has mutations in diagonal", async () => {
            let result = await DnaServiceInstance.hasValidMatrixDim([
                'ATGCGA',
                'CAGTGC',
                'TTATGT',
                'AGAAGG',
                'CCCCTA',
                'TCACTG'
            ]);
            
            assert.equal( 1, result );
        });

        it( "Has not mutations in diagonal", async () => {
            let result = await DnaServiceInstance.verifyDiagonal([
                'ATGCGA',
                'CCGTGC',
                'TTGTGT',
                'AGAAGG',
                'CCCCTA',
                'TCACTG'
            ]);

            assert.equal( 0, result );
        });

        it( "Has mutations in vertical", async () => {
            let result = await DnaServiceInstance.verifyVertical([
                'ACGCGA',
                'CCGTGC',
                'TTATGT',
                'AGAAGG',
                'ACCCTA',
                'ACACTG'
            ]);

            assert.equal( 3, result );
        });

        it( "Has not mutations in vertical", async () => {
            let result = await DnaServiceInstance.verifyVertical([
                'ATGCGA',
                'CAGTAC',
                'TTATGT',
                'AGAACG',
                'CCCCTA',
                'TCACTG'
            ]);

            assert.equal( 0, result );
        });

        it( "Has mutations in horizontal", async () => {
            let result = await DnaServiceInstance.verifyHorizontal([
                'ATGCGA',
                'CAGTGC',
                'TTATGT',
                'AGAAGG',
                'CCCCTA',
                'TCACTG'
            ]);

            assert.equal( 2, result );
        });

        it( "Has not mutations in horizontal", async () => {
            let result = await DnaServiceInstance.verifyHorizontal([
                'ATGCGA',
                'CAGTAC',
                'GTACGT',
                'AGTACG',
                'CGCATA',
                'TCACTG'
            ]);

            assert.equal( 0, result );
        });

        it( "Not mutations founded in DNA", async () => {
            const result = await DnaServiceInstance.hasMutation([
                'ATGCGA',
                'CAGTGC',
                'CTATAT',
                'AGACGG',
                'GCGTCA',
                'TCACTG'
            ]);

            assert.isFalse( result );
        });

        it( "Mutations are founded in DNA (Diagonal)", async () => {
            const result = await DnaServiceInstance.hasMutation([
                'ATGCCA',
                'CAGTAC',
                'TTATGT',
                'AGAAGG',
                'CAGCTA',
                'TCACTG'
            ]);

            assert.isTrue( result );
        });

        it( "Mutations are founded in DNA (Horizontal)", async () => {
            const result = await DnaServiceInstance.hasMutation([
                'ATGCAA',
                'CTGTGC',
                'TTATGT',
                'AGACTG',
                'CCCCTA',
                'TCACTG'
            ]);

            assert.isTrue( result );
        });

        it( "Mutations are founded in DNA (Vertical)", async () => {
            const result = await DnaServiceInstance.hasMutation([
                'ATGCGA',
                'CTGTGC',
                'CTATGT',
                'AGACGG',
                'CCGCTA',
                'CCACTG'
            ]);    
            
            assert.isTrue( result );
        });

        it( "Validate correct DNA structure", async () => {
            const result = await DnaServiceInstance.isValid([
                'ATGCGA',
                'CTGTGC',
                'CTATGT',
                'AGACGG',
                'GCGCTA',
                'TCACTG'
            ]);

            assert.isTrue( result );
        });

        it( "Validate incorrect DNA structure", async () => {
            const result = await DnaServiceInstance.isValid([
                'ATGCGA',
                'CTGTGC',
                'CTATGT',
                'AZACGG',
                'GCGCTA',
                'TCACTG'
            ]);

            assert.isFalse( result );
        });

        it( "Validate incorrect dim matrix DNA structure", async () => {
            const result = await DnaServiceInstance.isValid([
                'ATGCGA',
                'CTGTGC',
                'CTGT',
                'AGACGG',
                'GCGCTA',
                'TCACTG'
            ]);

            assert.isFalse( result );
        });

        it( "Validate correct dim matrix DNA structure", async () => {
            const result = await DnaServiceInstance.isValid([
                'ATGCGA',
                'CTGTGC',
                'CTGTCA',
                'AGACGG',
                'GCGCTA',
                'TCACTG'
            ]);

            assert.isTrue( result );
        });

        it( "Validate reject promise on invalid DNA structure", async () => {
            await assertThrowsAsync(async () => await DnaServiceInstance.hasMutation([
                'ATGCGA',
                'CTGTGC',
                'CTGTZA',
                'AGACGG',
                'GCGCTA',
                'TCACTG'
            ]), /Error/);
        });

    });
});