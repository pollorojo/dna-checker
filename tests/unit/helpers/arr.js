const assert = require( "chai" ).assert;
const mocha = require( "mocha" );
const arr = require( "../../../helpers/arr" );

mocha.describe( "Array Helper", () => {

    mocha.describe( "Handle array data", () => {
        it( "Array to dictionary map", () => {
            const dictionary = arr.arrayDictionaryMapper(['a','b','c','a','b','z','a']);
            
            assert.isObject( dictionary );
            assert.equal( 3, dictionary.a );
        });

        it( "String to dictionary map", () => {
            const dictionary = arr.stringDictionaryMapper('abcabza');
            
            assert.isObject( dictionary );
            assert.equal( 3, dictionary.a );
        });

        it( "Reverse matrix", () => {
            const matrix = arr.invertMatrix([
                [1,2,3,4],
                [4,3,2,1],
                [5,6,7,8],
                [8,7,6,5],
            ]);
            
            assert.isArray( matrix );
            assert.notStrictEqual( [
                [1,4,5,8],
                [2,3,6,7],
                [3,2,7,6],
                [4,1,8,5],
            ], matrix );
        });

        it( "Object has a gte value", () => {
            assert.isTrue(arr.hasGteValue({'a':3,'b':5,'c':1}, 5));
        });

        it( "Object has not a gte value", () => {
            assert.isFalse(arr.hasGteValue({'a':3,'b':5,'c':1}, 7));
        });

        it( "Count char repetition", () => {
            let data = [
                {'a':3,'b':4,'c':1},
                {'a':4,'b':2,'c':1},
                {'a':3,'b':2,'c':1}
            ];

            let result = arr.countCharRepetition(data, 4);
            assert.equal(2, result);
        });

        
    });

});