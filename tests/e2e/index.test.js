const app = require('../../app');
const request = require('supertest');

describe('POST /', () => {
    test('POST a DNA with mutation for verify', async (done) => {
        await request(app)
        .post('/mutation')
        .set('Content-Type',  'application/json')
        .send({
            dna: ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
        })
        .then((response) => {
            expect(response.statusCode).toBe(200);
            done();
        });
    });

    test('POST a DNA without mutation for verify', async (done) => {
        await request(app)
        .post('/mutation')
        .set('Content-Type',  'application/json')
        .send({
            dna: ["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"]
        })
        .then((response) => {
            expect(response.statusCode).toBe(403);
            done();
        });
    });

    test('POST a DNA with incorrent structure', async (done) => {
        await request(app)
        .post('/mutation')
        .set('Content-Type',  'application/json')
        .send({
            dna: ["ZZZCGA","CAGTGC","TTDDDT","AGACGG","GCGTCA","TCACTG"]
        })
        .then((response) => {
            expect(response.statusCode).toBe(400);
            done();
        });
    });

    test('POST without DNA structure', async (done) => {
        await request(app)
        .post('/mutation')
        .set('Content-Type',  'application/json')
        .send({})
        .then((response) => {
            expect(response.statusCode).toBe(400);
            done();
        });
    });
});