#!/bin/bash

RESET="\e[39m"
GREEN="\e[32m"
RED="\e[31m"

# Print info message
info () {
    echo -e $GREEN"$1"$RESET
    echo ""
}

info "Request example of DNA structure with mutations."

curl -i --header "Content-Type: application/json" \
  --request POST \
  --data '@test-1.json' \
  http://localhost:3000/mutation