# Classes Reference

## DnaService
*Class for make analizes of dna structures.*

### **hasMutation( dna: String[] )**
*Verify DNA mutation executing the verifyDiagonal, verifyHorizontal, verifyVertical methods.*

```javascript
var dna = ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"];

var service = new DnaService();
service.hasMutation(dna).then(
    (result) => {
        console.log(result); // print true
    }
);

```

### **isValid( dna: String[] )**
*Validate DNA structure*

```javascript
var dna = ["ATGCGA","CAACGC","TTATGT","AGAAGG","CCCCTA","TCACTG"];

var service = new DnaService();
var result = service.isValid(dna);
console.log(result); // print true
```

### **hasValidMatrixDim( dna: String[] )**
*Validate matrix dimension*

```javascript
var dna = ["ATGCGA","CGC","TTATGT","AGAAGG","CCCCTA","TCACTG"];

var service = new DnaService();
var result = service.hasValidMatrixDim(dna);
console.log(result); // print false
```

### **hasValidMatrixDim( dna: String[] )**
*Validate nitrogenous bases in string parameter*

```javascript
var dna = ["ATGZZZ","CGATGC","TTATGT","AGAAGG","CCCCTA","TCACTG"];

var service = new DnaService();
var result = service.hasValidNitrogenousBases(dna);
console.log(result); // print false
```

### **verifyDiagonal( dna: String[] )**
*Return a promise with boolean value when detect 4 repetition or more, of any char on diagonal.*

```javascript
var dna = ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"];

var service = new DnaService();
service.verifyDiagonal(dna).then(
    (result) => {
        console.log(result); // print true
    }
);

```

### **verifyHorizontal( dna: String[] )**
*Return a promise with boolean value when detect 4 repetition or more, of any char on horizontal lines.*

```javascript
var dna = ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"];

var service = new DnaService();
service.verifyHorizontal(dna).then(
    (result) => {
        console.log(result); // print true
    }
);

```

### **verifyVertical( dna: String[] )**
*Return a promise with boolean value when detect 4 repetition or more, of any char on vertical lines.*

```javascript
var dna = ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"];

var service = new DnaService();
service.verifyVertical(dna).then(
    (result) => {
        console.log(result); // print true
    }
);

```

----