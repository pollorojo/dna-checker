const arr = require('../helpers/arr');

/**
 * Class service to inspect DNA structure data.
 */
class DnaService {
    /**
     * This method executes the algorithms verifyDiagonal, verifyHorizontal and verifyVertical to find
     * mutations in de DNA matriz received and return a promise with a boolean value.
     * 
     * @param Array dna
     * @return Promise 
     */
    hasMutation ( dna ) {
        return new Promise(async (resolve, reject) => {
            if (!this.isValid(dna)) {
                reject("Error: Invalid DNA structure");
                return 1;
            }

            let count = 0;

            let resultDiagonal = await this.verifyDiagonal(dna);
            count = count + resultDiagonal;

            let resultHorizontal = await this.verifyHorizontal(dna);
            count = count + resultHorizontal;
            if (count > 1) {
                resolve(true)
                return 1;
            }

            let resultVertical = await this.verifyVertical(dna);
            count = count + resultVertical;
            if (count > 1) {
                resolve(true)
                return 1;
            }

            resolve(false);
        });
    }

    /**
     * Validate DNA structure
     * @param Array dna
     * @return boolean 
     */
    isValid(dna) {
        const matrix =  dna.map((item) => item.split(''));
        return (this.hasValidNitrogenousBases(dna.join("")) && this.hasValidMatrixDim(matrix));
    }

    /**
     * Validate matrix dimension
     * 
     * @param Array dna 
     * @return boolean
     */
    hasValidMatrixDim(dna) {
        for (let index = 0; index < dna.length; index++) {
            if (dna[index].length != dna.length) {
                return false;
            }
        }

        return true;
    }

    /**
     * Validate nitrogenous bases in string parameter
     * 
     * @param String sequence 
     * @return boolean
     */
    hasValidNitrogenousBases(sequence) {
        var regex = new RegExp("^[ACGT]*$");
        return regex.test(sequence);
    }

    /**
     * Diagonal verification of mutation in DNA structure.
     * 
     * This method ordering the values from each line getting one value in ascending index mode per line, and create
     * a dictionary structure with the number of repetitions per value.
     * 
     * @param Array dna 
     */
    verifyDiagonal( dna ) {
        return new Promise((resolve, reject) => {
            const values = dna.map( (el, idx) => (new String(el).slice(idx, idx+1)) );
            const dictionary = arr.arrayDictionaryMapper(values);
        
            let result = (arr.hasGteValue(dictionary, 4)) ? 1 : 0;
            resolve(result);
        });
    }

    /**
     * Horizontal verification of mutation in DNA structure.
     * 
     * This method mapping horizontal values to create a dictionary with repetitions number for each value in line,
     * then verify line per line if any has four or more repetitions.
     * 
     * @param Array dna 
     * @return Promise
     */
    verifyHorizontal( dna ) {
        return new Promise((resolve, reject) => {
            const values = dna.map(arr.stringDictionaryMapper);
        
            let result = arr.countCharRepetition(values, 4);

            resolve(result);
        });
    }

    /**
     * Vertical verification of mutation in DNA structure.
     * 
     * This method re-arrange matrix to alternate vertical values to horizontal values, and then use
     * horizontal verification algorithm.
     * 
     * @see verifyHorizontal
     * 
     * @param Array dna 
     * @return Promise
     */
    verifyVertical( dna ) {
        return new Promise((resolve, reject) => {
            const values = dna.map((sequence) => (new String(sequence)).split(''));
        
            const inverted = arr.invertMatrix(values)
                .map(arr.arrayDictionaryMapper);
                
            let result = arr.countCharRepetition(inverted, 4);
        
            resolve(result);
        });
    }
}

module.exports = DnaService;